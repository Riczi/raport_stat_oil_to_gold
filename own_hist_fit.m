function own_hist_fit(X, N)
        t = linspace(min(X), max(X) + 1, N + 1);
        % do obliczenia pola pod wykresem histogramu
        c = 0;       
        for k = 1:length(t) - 1      
            y(k) = sum(t(k) <= X & X < t(k+1));   
        endfor
        y = y/(length(X)*(t(2) - t(1)));
        c = sum(y.*(t(2) - t(1)));
        bar(t(1:end - 1), y, "histc"); 
        disp(c); 
endfunction