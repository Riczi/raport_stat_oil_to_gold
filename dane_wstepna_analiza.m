  pkg load nan
  % funkcja pobiera z .csv i analizuje
  % zestaw danych o maksymalnych dziennych cenach srebra w dolarach
  % za uncj� (oko�o 29 gram)
  % https://finance.yahoo.com/news/silver-price-prediction-prices-attempt-174631456.html?.tsrc=fin-srch
  % https://finance.yahoo.com/quote/CL%3DF?p=CL%3DF
  X = csvread ('GOLDdata.csv');
  
  X = X(:, 3);   % maksymalna cena - kolumna 3
  
  X = (nonzeros(X))';   % pozbywanie si� pustych kolumn
  max(X)
  % rysowanie danych
  
  plot(X);
  hold on;
  x = 252;
  plot([x,x],[1000,1800]);
  plot([2*x,2*x],[1000,1800]);
  plot([3*x,3*x],[1000,1800]);
  plot([4*x,4*x],[1000,1800]);
  xlabel('dni liczone od 21.04.2015');
  ylabel('cena w USD za uncj� z�ota');
  title('Wst�pny wykres dla maksymalnych dziennych cen z�ota')
  xlim([0, 1260]);
  % �rednia arytmetyczna
  mean(X, ['A'])
  mean(X, ['H'])
  mean(X, ['G'])
 
  % funkcje parametru k
  W = [];
  T = 1:1500;
  for k = T
    W = [W winsor(X, k)];
  endfor
  
  plot(T, W);
  xlim([0, 1500]);
  xlabel('parametr k')
  ylabel('�rednio USD za uncj�')
  title('�rednia winsorowska')
  t = [];
  for k = T
    t = [t trimmed(X, k)];
  endfor

  %plot(T, t);  
  %xlim([0, 1500]);
  %xlabel('parametr k')
  %ylabel('�rednio USD za uncj�')
  %title('�rednia ucinana')
