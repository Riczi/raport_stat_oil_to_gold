pkg load statistics

graphics_toolkit ("gnuplot")
X = csvread ('OILdata.csv');
  
X = X(:, 3);   % maksymalna cena - kolumna 3
  
X = (nonzeros(X))';   % pozbywanie się pustych kolumn'

mu = mean(X, 'a');
sig = std(X);
W = X;
f = @(x) (1/(sqrt(2*pi)*sig))*exp(-((x-mu).^2)/(2*sig^2));


subplot(311);
% dystrybuanta empiryczna
eF = empirical_cdf(T, W);
plot(T, eF)
ylim([0,1])
title("Dystrybuanta empiryczna");
xlabel("cena w USD za baryłkę ropy naftowej");
ylabel("dystrybuanta");

subplot(312);
% dystrybuanta teoretyczna
F = normcdf(T, mu, sig);
plot(T, F, 'r');
title("Dystrybuanta teoretyczna");
xlabel("cena w USD za baryłkę ropy naftowej");
ylabel("dystrybuanta");

subplot(313)
% odleglosc miedzy dystrybuantami 
d = max(abs(F - eF))		% ~0.06
plot(T, F, 'r');
hold on;
plot(T, eF);
hold off;
title(["Wykres dystrybuanty teoretycznej i empirycznej, odleglość maksymalna " num2str(d)]);
xlabel("cena w USD za baryłkę ropy naftowej");
ylabel("dystrybuanta");