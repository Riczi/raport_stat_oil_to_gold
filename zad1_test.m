pkg load statistics


W = r_normalny1(mu, sig, 1000000);
f = @(x) (1/(sqrt(2*pi)*sig))*exp(-((x-mu).^2)/(2*sig^2));

% histogram porownany z gestoscia teoretyczna
subplot(611);
hist(W, 100);
title(["Histogram ilosciowy nienormowany dla mu = " num2str(mu) ' sigma = ' num2str(sig)]);
subplot(612);
T = -4*mu:0.001:4*mu;
plot(T,  f(T));
%xlim([-4, 6]);
title("Gestosc teoretyczna rozkladu");
subplot(613);		% skalowany histogram i gestosc
hold on;
own_hist_fit(W, 100, 1);
plot(T, f(T), 'r', 'linewidth',  3);
xlim([-4, 4]);
title("Porownanie gestosci teoretycznej z histogramem skalowanym");
subplot(614);
% dystrybuanta empiryczna
eF = empirical_cdf(T, P);
plot(T, eF)
ylim([0,1])
title("Dystrybuanta empiryczna");
subplot(615);
% dystrybuanta teoretyczna
F = normcdf(T, mu, sig);
plot(T, F, 'r');
title("Dystrybuanta teoretyczna");

subplot(616)
% odleglosc miedzy dystrybuantami 
d = max(abs(F - eF))		% ~0.03
plot(T, F, 'r');
hold on;
plot(T, eF);
hold off;
title(["Wykres dystrybuanty teoretycznej i empirycznej, odleglosc maksymalna " num2str(d)]);

kolmogorov_smirnov_test (W, 'normal')

% test nie chce dzialac w OCTAVE.
%error: kolmogorov_smirnov_test: no normalcdf or normal_cdf function found
%error: called from
%    kolmogorov_smirnov_test at line 73 column 7
%    zad1_test at line 63 column 1
%probowalem dla roznych wywolan ale bezskutecznie
