pkg load statistics

%graphics_toolkit ("gnuplot")
X = csvread ('OILdata.csv');
  
X = X(:, 3);   % maksymalna cena - kolumna 3
  
X = (nonzeros(X))';   % pozbywanie się pustych kolumn'

Y = csvread ('GOLDdata.csv');
  
Y = X(:, 3);   % maksymalna cena - kolumna 3
  
Y = (nonzeros(X))';   % pozbywanie się pustych kolumn'

subplot(121)
boxplot(X)

subplot(122)
boxplot(Y)