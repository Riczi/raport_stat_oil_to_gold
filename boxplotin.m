pkg load statistics

graphics_toolkit ("gnuplot")
X = csvread ('OILdata.csv');
  
X = X(:, 3);   % maksymalna cena - kolumna 3
  
X = (nonzeros(X))';   % pozbywanie się pustych kolumn'

Y = csvread ('GOLDdata.csv');
  
Y = Y(:, 3);   % maksymalna cena - kolumna 3
  
Y = (nonzeros(Y))';   % pozbywanie się pustych kolumn'

subplot(121)
boxplot(X)
set(gca (), "xtick", [1 2], "xticklabel", {"ropa"})
ylabel("cena w USD za baryłkę ropy");
title ("Wykres pudełkowy dla danych z tabeli 1");
subplot(122)
boxplot(Y)
set(gca (), "xtick", [1 2], "xticklabel", {"złoto"})
ylabel("cena w USD za uncję złota");
title ("Wykres pudełkowy dla danych z tabeli 2");
