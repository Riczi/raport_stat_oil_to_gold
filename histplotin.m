pkg load statistics

graphics_toolkit ("gnuplot")
X = csvread ('OILdata.csv');
  
X = X(:, 3);   % maksymalna cena - kolumna 3
  
X = (nonzeros(X))';   % pozbywanie się pustych kolumn'

Y = csvread ('GOLDdata.csv');
  
Y = Y(:, 3);   % maksymalna cena - kolumna 3
  
Y = (nonzeros(Y))';   % pozbywanie się pustych kolumn'

%own_hist_fit3(X, Y,100);
%title('Wykres przeskalowanych przybliżeń histogramu');
%set(gca, 'XTickLabel', [])
T = 1000:0.001:1800;  % gold

TT = 0:0.0001:80;		% oil
plot(TT, empirical_cdf(TT, X))
hold on;
plot(TT, empirical_cdf(T, Y))
legend("przybliżenie dystrybuanty dla cen ropy", "przybliżenie dystrybuanty dla cen złota", 'location', 'northwest');
set(gca, 'XTickLabel', [])
ylabel("dystrybuanta empiryczna");