  X = csvread ('OILdata.csv');
  
  X = X(:, 3);   % maksymalna cena - kolumna 3
  
  X = (nonzeros(X))';   % pozbywanie się pustych kolumn
  max(X)
  % rysowanie danych
  
  plot(X);
  hold on;
  x = 252;
  plot([x,x],[0,100]);
  plot([2*x,2*x],[0,100]);
  plot([3*x,3*x],[0,100]);
  plot([4*x,4*x],[0,100]);
  xlabel('dni liczone od 21.04.2015');
  ylabel('cena w USD za bary³kê ropy naftowej');
  title('Wstêpny wykres dla maksymalnych dziennych cen ropy naftowej')
  #xlim([0, 1260]);