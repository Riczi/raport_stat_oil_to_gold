function T = trimmed(X, k)
  
  % script to calculate the winsor mean of X data sample
  % WARNING data set must be at least 3xk
  % parameter:
  % X - data vactor
  % k - parameter of winsor mean
  
  n = length(X);

  T = (1/(n - 2*k))*(sum(X(k+1:n-k)));
  
endfunction
