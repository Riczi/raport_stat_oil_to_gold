function W = winsor(X, k)
  
  % script to calculate the winsor mean of X data sample
  % WARNING data set must be at least 3xk
  % parameter:
  % X - data vactor
  % k - parameter of winsor mean
  
  n = length(X);

  W = (1./n).*((k + 1).*X(k+1) + sum(X(k+2:n-k-1)) + (k+1).*X(n-k));
  
endfunction
