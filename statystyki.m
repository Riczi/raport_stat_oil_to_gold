pkg load statistics


X = csvread ('OILdata.csv');
X = X(:, 3);   % maksymalna cena - kolumna 3
X = (nonzeros(X))'; 

Y = csvread ('GOLDdata.csv');
Y = Y(:, 3);   % maksymalna cena - kolumna 3
Y = (nonzeros(Y))'; 

disp("Wszystkie kwartyle X")
quantile(X)
disp("Wszystkie kwartyle Y")
quantile(Y)

disp("Wariancja X")
var(X)
var(Y)

std(X)
std(Y)

mean(X, 'a')
mean(X, 'h')
mean(X, 'g')

mean(Y, 'a')
mean(Y, 'h')
mean(Y, 'g')

axis ([0.5, 1.5]);
 boxplot (X);
 set(gca (), "xtick", [1 2], "xticklabel", {"girls", "boys"})
 title ("Wykres pude?kowy dla danych z tabeli 1");