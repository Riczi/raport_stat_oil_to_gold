  X = csvread ('OILdata.csv');
  
  X = X(:, 3);   % maksymalna cena - kolumna 3
  
  X = (nonzeros(X))';   % pozbywanie si� pustych kolumn
  max(X)
  % rysowanie danych
  
  loglog(X);
  hold on;
  xlabel('dni liczone od 21.04.2015');
  ylabel('cena w USD za bary�k� ropy naftowej/za uncj� z�ota');
  title('Por�wnanie wizualne obu wykres�w')
  xlim([0, 1260]);
  
  X = csvread ('GOLDdata.csv');
  
  X = X(:, 3);   % maksymalna cena - kolumna 3
  
  X = (nonzeros(X))';   % pozbywanie si� pustych kolumn
  max(X)
  % rysowanie danych
  
  plot(X-1000);
  hold on;
  xlim([0, 1260]);
  axis('off')
  legend('cena ropy naftowej', 'cena z�ota', "location", 'northwest');