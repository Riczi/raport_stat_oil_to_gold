pkg load statistics

graphics_toolkit ("gnuplot")
X = csvread ('GOLDdata.csv');
  
X = X(:, 3);   % maksymalna cena - kolumna 3
  
X = (nonzeros(X))';   % pozbywanie się pustych kolumn'

mu = mean(X, 'a');
sig = std(X);
W = X;
f = @(x) (1/(sqrt(2*pi)*sig))*exp(-((x-mu).^2)/(2*sig^2));

figure(1)
% histogram porownany z gestoscia teoretyczna
subplot(311);
hist(W, 100);
xlabel("cena w USD za uncję złota");
ylabel("ilość");
title("Histogram ilościowy nienormowany");
subplot(312);
T = 1000:0.001:1800;
plot(T,  f(T));
xlabel("cena w USD za uncję złota");
ylabel("gęstość");
title(["Gestośc teoretyczna rozkładu  dla mu = " num2str(mu) ' sigma = ' num2str(sig)]);

subplot(313);		% skalowany histogram i gestosc
hold on;
own_hist_fit(W, 100, 1);
plot(T, f(T), 'r', 'linewidth',  3);
xlabel("cena w USD za uncję złota");
title("Porownanie gęstości teoretycznej z histogramem skalowanym");

figure(2)
subplot(311);
% dystrybuanta empiryczna
eF = empirical_cdf(T, W);
plot(T, eF)
ylim([0,1])
title("Dystrybuanta empiryczna");
xlabel("cena w USD za uncję złota");
ylabel("dystrybuanta");

subplot(312);
% dystrybuanta teoretyczna
F = normcdf(T, mu, sig);
plot(T, F, 'r');
title("Dystrybuanta teoretyczna");
xlabel("cena w USD za uncję złota");
ylabel("dystrybuanta");

subplot(313)
% odleglosc miedzy dystrybuantami 
d = max(abs(F - eF))		% ~0.06
plot(T, F, 'r');
hold on;
plot(T, eF);
hold off;
title(["Wykres dystrybuanty teoretycznej i empirycznej, odleglość maksymalna " num2str(d)]);
xlabel("cena w USD za uncję złota");
ylabel("dystrybuanta");
