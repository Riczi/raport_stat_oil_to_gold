pkg load statistics


X = csvread ('GOLDdata.csv');
X = X(:, 3);   % maksymalna cena - kolumna 3
X = (nonzeros(X))'; 

Y = csvread ("OILdata.csv");
Y = Y(:, 3);   % maksymalna cena - kolumna 3
Y = (nonzeros(Y))'; 
%{
for i = 1:300
	W(i) = winsor(X, i);
endfor
plot(W)
title("Wykres średniej winsorowskiej dla cen złota");
xlabel("parametr k");
ylabel(' USD za uncję');
%}
for i = 1:400
	W(i) = winsor(Y, i);
endfor
plot(W)
xlabel("parametr k");
ylabel("USD za baryłkę");
title("Wykres średniej winsorowskiej dla cen ropy");
