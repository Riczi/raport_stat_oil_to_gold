function own_hist_fit2(X, Y, N)
		t = linspace(min(X), max(X) + 1, N + 1);
		t1 = linspace(min(Y), max(Y) + 1, N + 1);
		% do obliczenia pola pod wykresem histogramu

		c = 0;  
		for k = 1:length(t) - 1      
			y(k) = sum(t(k) <= X & X < t(k+1));   
		endfor

		c1 = 0;       
		for k = 1:length(t1) - 1      
			y1(k) = sum(t1(k) <= Y & Y < t1(k+1));   
		endfor

		y = y/(length(X)*(t(2) - t(1)));
		c = sum(y.*(t(2) - t(1)));
		plot(t(1:end - 1), y); 
		disp(c); 
		hold on;

		y1 = y1/(length(Y)*(t1(2) - t1(1)));
		c1 = sum(y1.*(t1(2) - t1(1)));
		plot(t(1:end - 1), y1); 
		disp(c1); 
		legend("przybliżenie histogramu dla cen ropy", "przybliżenie histogramu dla cen złota")
endfunction